## Dotfiles README

This repo contains configuration files that I use across Linux & macOS, managed using [chezmoi](https://www.chezmoi.io/).

The list of configurations that I'm currenly managing through this repo are:

* Bash Shell
* Git
* Ansible
* Sublime Merge
* lazygit
* Emacs (more specifically the Doom Emacs framework) configuration and dependent shell scripts covering:
  * Custom fonts
  * Emacs tabs (centaur-tabs)
  * Emacs spellcheck (hunspell)
  * org-mode
  * Custom elisp functions.
* ZSH Shell with the following frameworks:
  * zprezto
  * powerlevel10k.
* Brew package manager.
